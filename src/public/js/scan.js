const bittrexObj = {};
const poloniexObj = {};
const bithumbObj = {};
let krw_usd = 1;
$(function () {
	setratio();
	var socket = io();
	socket.on('bittrex', function(data){
		if(!(data.pair in bittrexObj)){
			bittrexObj[data.pair] = {};
		}
		bittrexObj[data.pair]['price'] = data.price;
		bittrexObj[data.pair]['bid'] = data.bid;
		bittrexObj[data.pair]['ask'] = data.ask;
		updatePrice('bittrex__', data.pair, data.price);
	});
	socket.on('poloniex', function(data){
		if(!(data.pair in poloniexObj)){
			poloniexObj[data.pair] = {};
		}
		poloniexObj[data.pair]['price'] = data.price;
		poloniexObj[data.pair]['bid'] = data.bid;
		poloniexObj[data.pair]['ask'] = data.ask;
		updatePrice('poloniex__', data.pair, data.price);
	});
	const bithumbInterval = setInterval(()=>{
		$.ajax({
			url: "https://api.bithumb.com/public/ticker/all",
			// context: document.body
		}).done(function(res) {
			for(key in res.data){
				if(key == 'date') continue;
				let price = parseFloat(res.data[key].average_price)*krw_usd;
				let bid = parseFloat(res.data[key].sell_price)*krw_usd;
				let ask = parseFloat(res.data[key].buy_price)*krw_usd;
				if(!(key in bithumbObj)){
					bithumbObj[key] = {};
				}
				bithumbObj[key]['price'] = price;
				bithumbObj[key]['bid'] = bid;
				bithumbObj[key]['ask'] = ask;
				updatePrice('bithumb__', key, price);
				// let nameAttr = 'bithumb__'+key;
				// let tempDOM = $("input[name=" + nameAttr + "]");
				// if(tempDOM.length>0){
				// 	let temp = tempDOM.val();
				// 	if(temp>bithumbObj[key]){ 
				// 		tempDOM.removeClass('green_text');
				// 		tempDOM.addClass('red_text'); 
				// 	}
				// 	else{
				// 		tempDOM.removeClass('red_text'); 
				// 		tempDOM.addClass('green_text');
				// 	}
				// 	tempDOM.val(bithumbObj[key]);
				// }
			}
		});
	}, 1000);
	const ratioInterval = setInterval(setratio(), 60000);
});
function updatePrice(pre, pair, price){
	let nameAttr = pre+pair;
	let tempDOM = $("input[name=" + nameAttr + "]");
	if(tempDOM.length>0){
		let temp = tempDOM.val();
		if(temp>price){ 
			tempDOM.removeClass('green_text');
			tempDOM.addClass('red_text'); 
		}
		else{
			tempDOM.removeClass('red_text'); 
			tempDOM.addClass('green_text');
		}
		tempDOM.val(price);
	}
}
function setratio(){
	$.ajax({
		url: "http://api.fixer.io/latest?base=KRW",
	}).done(function(res) {
		// console.log(res.rates.USD);
		krw_usd = res.rates.USD
	});
}