import express from 'express';
import http from 'http';
import sio from 'socket.io';
import ajax from 'ajax-request';
import autobahn from 'autobahn';
import bittrex from 'node.bittrex.api';

import routes from './routes';

const app = express();
const server = http.createServer(app);
const io = sio.listen(server);

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

const port = process.env.PORT || 7890;

// io.on('connection', (socket)=>{
// 	console.log('a user connected');
// 	socket.on('disconnect', ()=>{
// 		console.log('user disconnected');
// 	});
	
// });

///////////////bittrex socket connect///////////////////

const websocketsclient = bittrex.websockets.listen( function( data ) {
	if (data.M === 'updateSummaryState') {
		data.A.forEach(function(data_for) {
			data_for.Deltas.forEach(function(marketsDelta) {
				let pair = marketsDelta.MarketName;
				let last = parseFloat(marketsDelta.Last);
				let bid = parseFloat(marketsDelta.Bid);
				let ask = parseFloat(marketsDelta.Ask);
				let bittrexObj = {};
				bittrexObj['pair'] = pair;
				bittrexObj['price'] = last;
				bittrexObj['bid'] = bid;
				bittrexObj['ask'] = ask;
				io.emit('bittrex', bittrexObj);
			});
		});
	}
});
// websocketsclient.serviceHandlers.reconnecting = function (retry) {
// 	console.log('reconnecting');
// 	return false;
// }
///////////////////////////////////////////////////////////
(function ticker() {
	const connection = new autobahn.Connection({
		url: 'wss://api.poloniex.com',
		realm: 'realm1',
		max_retries: -1
	});

  	connection.onopen = (session)=>{
  		console.log('connected to poloniex')
	    const marketEvent = (args,kwargs)=>{
	    	let pair = args[0];
	    	let last = parseFloat(args[1]);
	    	let bid = parseFloat(args[3]);
	    	let ask = parseFloat(args[2]);
	    	let poloniexObj = {}
	    	poloniexObj['pair'] = pair;
	    	poloniexObj['price'] = last;
	    	poloniexObj['bid'] = bid;
	    	poloniexObj['ask'] = ask;
	    	io.emit('poloniex', poloniexObj);
		}
	    session.subscribe('ticker', marketEvent);

	    setTimeout(function () {
	      connection.close();
	    }, 300000);

	};

	connection.open();

	setTimeout(ticker, 300200);
})();

// const bithumbInterval = setInterval(()=>{
// 	ajax({
// 		url: 'http://api.bithumb.com/public/ticker/all',
// 		method: 'GET',
// 	}, function(err, res, body) {
// 		console.log(body);
// 	});
// }, 1000);
/////////////server////////////
routes(app);

server.listen(port, '0.0.0.0', ()=>{
	console.log('\n' + '**********************************');
	console.log('SCAN Server listening on port ' + port);
	console.log('**********************************' + '\n');
});
